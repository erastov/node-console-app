#!/usr/bin/env node

const program = require('commander');

class CardDeck {
  static cardMap(card) {
    const cMap = {
      2: 'Two',
      3: 'Three',
      4: 'Four',
      5: 'Five',
      6: 'Six',
      7: 'Seven',
      8: 'Eight',
      9: 'Nine',
      10: 'Ten',
      'J': 'Jack',
      'Q': 'Queen',
      'K': 'King',
      'A': 'Ace',
      's': 'Spades♠',
      'c': 'clubs♣',
      'd': 'diamonds♦',
      'h': 'hearts♥'
    };

    return cMap[card];
  }

  constructor() {
    this.deck = [];

    for (let i = 2; i <= 10; i++) {
      this.deck.push(`${i}c`);
      this.deck.push(`${i}d`);
      this.deck.push(`${i}h`);
      this.deck.push(`${i}s`);
    }

    const topCards = ['J', 'Q', 'K', 'A'];

    topCards.forEach(cartName => {
      this.deck.push(`${cartName}c`);
      this.deck.push(`${cartName}d`);
      this.deck.push(`${cartName}h`);
      this.deck.push(`${cartName}s`);
    });

    this.deck = this.deck.sort(() => 0.5 - Math.random());
  }

  getRandomCard() {
    return this.deck.pop();
  }

  static getCardName(card) {
    const cardNumber = parseInt(card);

    if (isNaN(cardNumber)) {
      return `${CardDeck.cardMap(card[0])} of ${CardDeck.cardMap(card[1])}`;
    }

    return `${CardDeck.cardMap(card[0])} of ${CardDeck.cardMap(card.slice(-1))}`;
  }

  static getCardValue(card) {
    const cardNumber = parseInt(card);

    if (isNaN(cardNumber)) {
      const topCardName = cardNumber[0];
      return topCardName === 'A' ? 11 : 10;
    }

    return cardNumber;
  }
}

class Player {
  constructor(name) {
    this.name = name;
    this.cards = [];
    this.result = null;
  }

  takeCard(card) {
    this.cards.push(card);
    return this;
  }

  fillResult() {
    this.result = this.cards.reduce((acc, value) => acc + CardDeck.getCardValue(value), 0);
    return this;
  }

  static getPlayerName(i, playersNumber) {
    switch (i) {
      case 1:
        return 'Your';
      case (playersNumber + 2):
        return 'Dealer';
      default:
        return `Player ${i}`;
    }
  };
}

program
  .version('0.0.1')
  .command('newGame <playersNumber>')
  .action(playersNumber => {
    const maxPlayersNumber = 4;
    const cardDeck = new CardDeck();
    playersNumber = Number(playersNumber);

    if (isNaN(playersNumber)) {
      console.log('You have to enter number of players.');
      return;
    }

    if (playersNumber > maxPlayersNumber) {
      console.log(`Players number can not be more than ${maxPlayersNumber}`);
      return;
    }

    console.log(`Playing a game of Blackjack with ${playersNumber + 2} players!`);

    const players = [];
    const results = [];

    for (let i = 1; i <= playersNumber + 2; i++) {
      const name = Player.getPlayerName(i, playersNumber);
      const player = new Player(name);

      const card1 = cardDeck.getRandomCard();
      const card2 = cardDeck.getRandomCard();

      player.takeCard(card1).takeCard(card2).fillResult();

      console.log(`${name}'s hand: ${CardDeck.getCardName(card1)}, ${CardDeck.getCardName(card2)} (total = ${player.result})`);

      players.push(player);
    }

    const winners = players.reduce((acc, player) => {

      if (acc.result === player.result) {
        acc.players.push(player);
        return acc;
      }

      if (acc.result > player.result) {
        return acc;
      }

      acc.result = player.result;
      acc.players = [player];

      return acc;
    }, {result: null, players: []});


    const playersNames = winners.players.reduce((acc, player) => {
      acc.push(player.name);
      return acc;
    }, []);

    if (playersNames.length > 1) {
      const lastPlayer = playersNames.pop();
      console.log(`The winners are ${playersNames.join(', ')} and ${lastPlayer}!`);
    } else {
      console.log(`The winner is ${playersNames[0]}!`);
    }
  });

program.parse(process.argv);